# This file is part of cfpeek -*- autotest -*-
# Copyright (C) 2011-2021 Sergey Poznyakoff
#
# Cfpeek is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Cfpeek is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with cfpeek.  If not, see <http://www.gnu.org/licenses/>.

AT_SETUP([Guile: listing])
AT_KEYWORDS([guile listing])

CFPEEK_TEST_GUILE([
user smith;
group mail;
pidfile "/var/run/example";

logging {
    facility daemon;
    tag example;
}

program a {
    command "a.out";
    logging {
        facility local0;
        tag a;
    }
}

program b {
    command "b.out";
    wait yes;
    pidfile /var/run/b.pid;
}
],
[],
[cfpeek -e '(display node)(newline)'],
[EX_OK],
[#<node .user: "smith">
#<node .group: "mail">
#<node .pidfile: "/var/run/example">
#<node .logging.facility: "daemon">
#<node .logging.tag: "example">
#<node .program="a".command: "a.out">
#<node .program="a".logging.facility: "local0">
#<node .program="a".logging.tag: "a">
#<node .program="b".command: "b.out">
#<node .program="b".wait: "yes">
#<node .program="b".pidfile: "/var/run/b.pid">
])

AT_CLEANUP
