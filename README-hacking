These notes intend to help people working on the GIT version of cfpeek.
See end of file for copying conditions.

* Requirements

If you have taken the sources from GIT you will need the following
packages to build cfpeek.  I don't make any extra effort to accommodate
older versions of these packages, so please make sure that you have the
latest stable version.

- Automake <http://www.gnu.org/software/automake/>
- Autoconf <http://www.gnu.org/software/autoconf/>
- Bison <http://www.gnu.org/software/bison/>
- Flex <http://flex.sourceforge.net/>
- M4 <http://www.gnu.org/software/m4/>
- Git <http://git.or.cz>
- Texinfo <http://www.gnu.org/software/texinfo>

* Bootstrapping

Obviously, if you are reading these notes, you did manage to check out
cfpeek from GIT. The next step is to get other files needed to build,
which are extracted from other source packages: 

1. Change to the source tree directory

   cd cfpeek

2. Run

   ./bootstrap 

Once done, proceed as described in the file README (section
Building).


* Copyright information

Copyright (C) 2011-2021 Sergey Poznyakoff

   Permission is granted to anyone to make or distribute verbatim copies
   of this document as received, in any medium, provided that the
   copyright notice and this permission notice are preserved,
   thus giving the recipient permission to redistribute in turn.

   Permission is granted to distribute modified versions
   of this document, or of portions of it,
   under the above conditions, provided also that they
   carry prominent notices stating who last changed them.


Local Variables:
mode: outline
paragraph-separate: "[ 	]*$"
version-control: never
End:
